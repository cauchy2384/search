package pipeline

import (
	"bufio"
	"io"
	"net/http"
	"runtime"
	"sync"
	"sync/atomic"
)

func SearchWord(urls <-chan string, word string) int {
	return SearchWordWorkers(urls, word)
}

// common

func fetchNetHTTP(url string) (io.ReadCloser, error) {
	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	return res.Body, nil
}

func countWords(reader io.Reader, word string) (cnt int32) {
	scanner := bufio.NewScanner(reader)
	scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		if scanner.Text() == word {
			cnt++
		}
	}
	return cnt
}

// workers do fetch and search

func SearchWordWorkers(urls <-chan string, word string) int {
	var cnt int32

	workersNum := runtime.NumCPU()

	wg := sync.WaitGroup{}
	wg.Add(workersNum)
	for i := 0; i < workersNum; i++ {
		go workNetHttp(&wg, urls, word, &cnt)
	}
	wg.Wait()

	return int(cnt)
}

func workNetHttp(wg *sync.WaitGroup, urls <-chan string, word string, cnt *int32) {
	defer wg.Done()
	for url := range urls {
		res, _ := fetchNetHTTP(url)
		add := countWords(res, word)
		atomic.AddInt32(cnt, add)
	}
}
